import React from 'react'
import styles from './Forum.module.scss'
import Navigation from "./Navigation";
import Card from "./Card";
import ArticleApi from "../classes/api/PostApi";
import { IPost } from "../classes/models/IPost";

interface IProps {
    logged: boolean;
}

const Forum = (props: IProps) =>{
    const [news, setNews] = React.useState<IPost[]>([]);
    const [title, setTitle] = React.useState("");
    const [content, setContent] = React.useState("");

    React.useEffect(() => {
        ArticleApi.getArticles().then(result => setNews(result));
    }, []);

    return (
        <div className={styles.page}>
            <Navigation />
            <div className={styles.content}>
                <div className={styles.cards}>
                    {news.map((article: IPost) => {
                        return <Card post={article} />;
                    })}
                </div>
                <div className={props.logged ? styles.add : styles.none}>
                    <textarea
                        cols={39}
                        rows={12}
                        onChange={event => {
                            setContent(event.target.value);
                        }}
                    ></textarea>
                    <button
                        className={styles.button}
                        onClick={() => {
                            const newArticle: IPost = { user: localStorage.getItem("user") as string, content, date: new Date().toDateString() };
                            setNews(prevState => [...prevState, newArticle]);
                            setTitle("");
                            setContent("");
                            ArticleApi.publishArticle(newArticle);
                        }}
                    >
                        Отправить
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Forum