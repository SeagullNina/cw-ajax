import React from "react";
import styles from './Card.module.scss'
import {IPost} from "../classes/models/IPost";
interface IProps {
    post: IPost
}

const Card = (props: IProps) =>
{
    return <div className={styles.card}>
        <div className={styles.name}>{props.post.user}</div>
        <div className={styles.info}>{props.post.content}</div>
        <div className={styles.date}>{props.post.date}</div>
    </div>
}
export default Card