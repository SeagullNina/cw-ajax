import React from 'react'
import styles from './Resources.module.scss'
import Navigation from "./Navigation";
const Resources = () =>{
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.info}>
            <img alt="" src="../logo.png" className={styles.img} />
        </div>
    </div>
}

export default Resources