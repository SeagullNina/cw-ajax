import React from 'react'
import styles from './Abilities.module.scss'
import Navigation from "./Navigation";

const Abilities = () =>{
    return <div className={styles.page}>
        <Navigation/>
        <div className={styles.info}>
            AJAX — это коллекция технологий, существующих с момента появления Web. А вот и возможности, предоставляемые AJAX (как это представил Джис Джеймс Гаррет (Jesse James Garrett), он первым ввел термин 'AJAX' для асинхронного JavaScript + XML):
            <ul>
                <li>Стандартно-базированная презентация с использованием XHTML и CSS;</li>
                <li>Динамическое отображение и взаимодействие с использованием объектной модели документа;</li>
                <li>Взаимообмен данными и манипуляция с задействованием XML и XSLT;</li>
                <li>Асинхронное извлечение данных с использованием XMLHttpRequest;</li>
                <li>JavaScript, связывающий все вместе.</li>
            </ul>
            Вкратце AJAX позволяет писать быстрореагирующие веб-приложения, в которых не нужно постоянно обновлять страницы. AJAX — простая технология, поддерживаемая всеми основными браузерами. Как можно вкратце отметить, единственным предварительным условием для внедрения AJAX является знание JavaScript.
        </div>
    </div>
}

export default Abilities