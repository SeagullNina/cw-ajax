import React from 'react'
import styles from './History.module.scss'
import Navigation from "./Navigation";
const History = () =>{
    return <div className={ styles.page}>
        <Navigation/>
        <div className={styles.info}>
            Впервые термин AJAX был публично использован 18 февраля 2005 года в статье Джесси Джеймса Гарретта (Jesse James Garrett) «Новый подход к веб-приложениям». Гарретт придумал термин, когда ему пришлось как-то назвать новый набор технологий, предлагаемый им клиенту.
            Однако в той или иной форме многие технологии были доступны и использовались гораздо раньше, например в подходе «Remote Scripting», предложенном компанией Microsoft в 1998 году, или с использованием HTML-элемента IFRAME, появившегося в Internet Explorer 3 в 1996 году.
            AJAX стал особенно популярен после использования его компанией Google в сервисах Gmail, Google Maps и Google Suggest.
            <br/>
            История технологии AJAX, которая, казалось бы, мгновенно завоевала огромную популярность, похожа на историю других прорывных технологий. Хотя кажется, что эта технология появилась из ниоткуда, на самом деле для этого потребовалось определенное время. Несколько лет исследований в области Web-разработки привели к созданию инструментов и подходов, которые распространялись под «брендом» AJAX. Начиная с эры DHTML в период первоначального Интернет-бума и на протяжении "темных" лет после массового краха доткомов разработчики по всему миру открывали для себя неожиданные возможности JavaScript, открывающие новые пути для разработки Web-приложений.
        </div>
    </div>
}

export default History