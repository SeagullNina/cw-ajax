import React from "react";
import styles from "./MainPage.module.scss";
import { Link } from "react-router-dom";
import AuthForm from "./Main/AuthForm";
import RegForm from "./Main/RegForm";
import Navigation from "./Navigation";

interface IProps {
  login: boolean;
}

const MainPage = (props: IProps) => {
  const { login } = props;
  return (
    <div className={styles.mainPage}>
      <Navigation />
      <div>
        <div className={styles.auth}>
          <div className={styles.toggle}>
            <Link to="/registration/">
              <input
                id={!props.login ? "toggle-on" : "toggle-off"}
                className={styles.toggle}
                name="toggle"
                value={`${!props.login}`}
                type="radio"
                checked={!props.login}
              />
              <label
                htmlFor={!props.login ? "toggle-on" : "toggle-off"}
                className={styles.btnCreate}
              >
                Создать
              </label>
            </Link>
            <Link to="/auth/">
              <input
                id={props.login ? "toggle-on" : "toggle-off"}
                className={styles.toggle}
                name="toggle"
                value={`${props.login}`}
                type="radio"
                checked={props.login}
              />
              <label
                htmlFor={props.login ? "toggle-on" : "toggle-off"}
                className={styles.btnEnter}
              >
                Войти
              </label>
            </Link>
          </div>
          {login ? <AuthForm /> : <RegForm />}
        </div>
      </div>
    </div>
  );
};

export default MainPage;
