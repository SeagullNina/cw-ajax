import React from "react";
import styles from "./RegForm.module.scss";
import UserApi from "../../classes/api/UserApi";

const RegForm = () => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [errorPassword, setErrorPassword] = React.useState(false);
  const [errorEmpty, setErrorEmpty] = React.useState(false);
  const [email, setEmail] = React.useState<string>("");
  const [password, setPassword] = React.useState<string>("");
  const [yetPassword, setYetPassword] = React.useState<string>("");
  return (
    <div className={styles.page}>
      <div className={styles.header}>Создать аккаунт</div>
      <div className={styles.form}>
        <span>Email</span>
        <input
          className={styles.input}
          type="text"
          placeholder={"Введите вашу почту"}
          onChange={event => setEmail(event.target.value)}
        />
        <span>Пароль</span>
        <input
          className={styles.input}
          type="password"
          placeholder={"Введите ваш пароль"}
          onChange={event => setPassword(event.target.value)}
        />
        <span>Повторите пароль</span>
        <input
          className={styles.input}
          type="password"
          placeholder={"Введите ваш пароль"}
          onChange={event => setYetPassword(event.target.value)}
        />
        <span className={errorPassword ? styles.error : styles.none}>
          Пароли не совпадают.
        </span>
        <span className={errorEmpty ? styles.error : styles.none}>
          Пожалуйста, заполните все поля.
        </span>
        <input
          disabled={loading}
          className={styles.button}
          type="button"
          value="Зарегистрироваться"
          onClick={() => {
            if (email && password === yetPassword) {
              setLoading(true);
              UserApi.registration({ email, password }).then(() => {
                setLoading(false);
                setErrorPassword(false);
                setErrorEmpty(false);
                window.location.href = "/";
                localStorage.setItem("logged", "1");
                localStorage.setItem("user", email)
              });
            } else if (password !== yetPassword) {
              setErrorPassword(true);
              return;
            } else if (!email || !password || !yetPassword) {
              setErrorPassword(false);
              setErrorEmpty(true);
              return;
            }
          }}
        />
      </div>
    </div>
  );
};

export default RegForm;
