import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Field from './Field'
import Abilities from './Abilities'
import Resources from "./Resources";
import History from "./History";
import Forum from "./Forum";
import Main from './MainPage'

const Routes = React.memo(() => {
    let log = !!localStorage.getItem('logged');
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/auth/">
                    <Main login={true} />
                </Route>
                <Route path="/registration/">
                    <Main login={false} />
                </Route>
                <Route exact path="/">
                    <Field/>
                </Route>
                <Route path="/abilities/">
                    <Abilities/>
                </Route>
                <Route path="/resources/">
                    <Resources/>
                </Route>
                <Route path="/history/">
                    <History/>
                </Route>
                <Route path="/forum/">
                    <Forum logged={log}/>
                </Route>
            </Switch>
        </BrowserRouter>
    )
})

export default Routes
