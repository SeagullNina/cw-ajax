import React from "react";
import styles from "./Navigation.module.scss";
import { NavLink } from "react-router-dom";
import classNames from "classnames";

const Navigation = () => {
  return (
    <div className={styles.mainPage}>
      <div className={styles.navigation}>
        <div className={styles.menuBlock}>
          <NavLink
            exact
            to="/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Область применения</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/abilities/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Возможности</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/history/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>История развития</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/resources/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Полезные ресурсы</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/forum/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Форум</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
              to="/auth/"
              className={classNames(
                  styles.link,
                  window.location.pathname === "/registration/"
                      ? styles.selected
                      : ""
              )}
              activeClassName={styles.selected}
          >
            <span className={styles.menuName}>Вход / Регистрация</span>
          </NavLink>
        </div>
      </div>
    </div>
  );
};

export default Navigation;
