import axios, { AxiosInstance } from 'axios'
import { IPost } from '../models/IPost'

class ArticleApi {
    private http: AxiosInstance = axios.create({
        baseURL: 'https://cv-ajax-backend.herokuapp.com/api/article',
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json;charset=utf-8',
        },
    })

    public getArticles = async (): Promise<IPost[]> =>{
        const fetchedArticles = await this.http.get("")
        return fetchedArticles.data;
    }
    public publishArticle = async (post: IPost) =>{
        return await this.http.post("", post)
    }
}

export default new ArticleApi()