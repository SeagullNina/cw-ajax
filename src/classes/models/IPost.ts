export interface IPost {
    user: string,
    content: string,
    date: string
}