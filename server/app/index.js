const userRouter = require('./user/routes');
const articleRouter = require('./article/routes');

module.exports = (app) => {
    app.use('/api', userRouter);
    app.use('/api/article', articleRouter);

    app.get('/', (req, res) => res.send({success: true,data:Boolean(Math.round(Math.random()))}));
};
