const express = require('express');
const router = express.Router();

require('./model');
const controller = require('./controllers');


router
  .post("/auth", controller.login)
  .post("/registration", controller.register)

module.exports = router;
