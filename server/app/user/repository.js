const User = require('./model');

exports.saveUser = (data, saveCb) => {
    const user = new User(data);
    return user.save(saveCb);
};

exports.findUser = async (email, password) => await User.findOne({email, password});
