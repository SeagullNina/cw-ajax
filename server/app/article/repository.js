const Article = require('./model');

exports.saveArticle = (data, saveCb) => {
    const article = new Article(data);
    return article.save(saveCb);
};

exports.findArticles = async (userId, cb) => await Article.find({userId}, cb);
