const repository = require('./repository');

exports.addArticle = (req, res) => {
  const data = req.body;
    return repository
    .saveArticle(data, (err, Article) => {
        if (err) {
            return res.validationError(err);
        }
        return res.send(Article);
    })
}

exports.getArticles = async (req, res) => {
  const { userId } = req.query;
  return repository
    .findArticles(userId, (err, Articles) => {
        if (err) {
            return res.validationError(err);
        }
        return res.send(Articles);
    })
}
