const express = require('express');
const router = express.Router();

require('./model');
const controller = require('./controllers');


router
  .post("/", controller.addArticle)
  .get("/", controller.getArticles)

module.exports = router;
