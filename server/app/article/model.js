const mongoose = require('mongoose');

const { Schema } = mongoose;

const definition = {
    title:{
        type: String
    },
    content:{
        type: String
    }
};

const options = {
    timestamps: true
};

const ArticleSchema = new Schema(definition, options);

module.exports = mongoose.model('Article', ArticleSchema);